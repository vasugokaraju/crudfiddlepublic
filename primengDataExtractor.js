// READ ME
// THIS SCRIPT IS TO EXTRACT PRIMENG CONTROLS RELATED DATA.
// GO TO https://www.primefaces.org/primeng/#/inputgroup
// OPEN DEBUG WINDOW (F12)
// CLEAR CONSOLE
// COPY THIS ENTIRE SCRIPT AND PASTE IT IN BROWSER CONSOLE AND PRESS ENTER
// IT TAKES FEW SECONDS TO COLLECT DATA
// EXTRACTED DATA WILL BE GIVEN AS JSON STRING
// SCROLL ALL THE WAY DOWN IN CONSOLE PANEL AND CLICK ON 'COPY' OPTION TO GET ENTIRE JSON
// IF SOME OF THE DATA IS NOT COLLECTED, TRY AGAIN



// ========== Jquerify a page
var script = document.createElement('script');
script.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js";
document.getElementsByTagName('head')[0].appendChild(script);

$("#menu_input").click();

// ========== Extract data from primeng page
var INTERVAL_DELAY = 2000;
var i=0;
var links=[];
var intervalRef;
var data=[];
var ignoreList='InputGroup'

$('.submenushow a').each( (indx, elmt) => {
    links.push( {label:$(elmt).text().replace(/● /,''), url:'https://www.primefaces.org/primeng/#/' + $(elmt).attr('href').substr(2)} );
});


intervalRef = setInterval(function(){
    if(links.length == 0) return;

    var link = links[i];
    window.location = link.url;

    setTimeout(()=>{
        var elmtJSON = {
            pageURL: window.location.href,
            import: $('pre.language-typescript code.language-typescript').first().text().trim(),
            label: link.label,
            value: link.label,
            tag: $('code.language-markup span.token.tag span.token.tag').first().text().substr(1),
            properties: getProps(),
            events: getEvents(),
            styling: getStyling()
        };

        elmtJSON.properties.shift();  // Remove the table header row
        elmtJSON.events.shift();  // Remove the table header row
        elmtJSON.styling.shift();  // Remove the table header row

        console.log(i,link.label, elmtJSON.value)

        if(ignoreList.indexOf(link.label) == -1) data.push(elmtJSON);

        if(i == links.length-1) {
            clearInterval(intervalRef);
            console.log( JSON.stringify(data, null, ' '));
        }

        i++;
    }, 500)


},INTERVAL_DELAY)



function getProps(){
    return $("h3:contains('Properties')").nextAll().find('table').first().find('tr').map( (i,val) => {
        var $elem = $(val);
        return {
            name: $($elem).find('td').first().text(),
            type: $($elem).find('td:nth-child(2)').text(),
            default: $($elem).find('td:nth-child(3)').text(),
            description: $($elem).find('td:nth-child(4)').text(),
        }
    }).get();
}

function getEvents(){
    return $("h3:contains('Events')").nextAll().find('table').first().find('tr').map( (i,val) => {
        var $elem = $(val);
        return {
            name: $($elem).find('td').first().text(),
            parameters: $($elem).find('td:nth-child(2)').text(),
            description: $($elem).find('td:nth-child(3)').text(),
        }
    }).get();
}

function getStyling(){
    return $("h3:contains('Styling')").nextAll().find('table').first().find('tr').map( (i,val) => {
        var $elem = $(val);
        return {
            name: $($elem).find('td').first().text(),
            element: $($elem).find('td:nth-child(2)').text()
        }
    }).get();
}

// ====================

